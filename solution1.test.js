const test = require("ava");
const solution1 = require("./solution1");

const validCaseNoRegex = /^(?:[1-9]|9)$/;
const validTestCaseRegex =
  /^([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)$/;

test("Check isValidInput for number Of test case. (valid value)", (t) => {
  const checkValidInput = solution1.isValidInput(validCaseNoRegex, 3);
  t.is(checkValidInput, true);
});
test("Check isValidInput for number Of test case. (invalid value)", (t) => {
  const checkValidInput = solution1.isValidInput(validCaseNoRegex, 10);
  t.is(checkValidInput, false);
});
test("Check isValidInput for valid game test case value.", (t) => {
  const checkValidInput = solution1.isValidInput(validTestCaseRegex, "9 2 5 4");
  t.is(checkValidInput, true);
});
test("Check isValidInput for invalid game test case value.", (t) => {
  const checkValidInput = solution1.isValidInput(validTestCaseRegex, "d 2 5 4");
  t.is(checkValidInput, false);
});

test("Check valid player's age.", (t) => {
  const isValidAges = solution1.isValidAges(3, 8);
  t.is(isValidAges, true);
});
test("Check invalid player's age. Ages are same.", (t) => {
  const isValidAges = solution1.isValidAges(8, 8);
  t.is(isValidAges, false);
});
test("Check invalid player's age. Age is 0.", (t) => {
  const isValidAges = solution1.isValidAges(0, 8);
  t.is(isValidAges, false);
});

test("Check older player will win base on remaing move.", (t) => {
  const checkOlderPlayerWin = solution1.checkOlderPlayerWin(2, 4);
  t.is(checkOlderPlayerWin, true);
});
test("Check older player will lose base on remaing move.", (t) => {
  const checkOlderPlayerWin = solution1.checkOlderPlayerWin(1, 5);
  t.is(checkOlderPlayerWin, false);
});

test("Check older player1 win based on isOlderPlayerWin and players age.", (t) => {
  const checkPlayer1Win = solution1.checkPlayer1Win(true, 9, 5);
  t.is(checkPlayer1Win, 1);
});
test("Check older player1 lose based on isOlderPlayerWin and players age.", (t) => {
  const checkPlayer1Win = solution1.checkPlayer1Win(false, 10, 9);
  t.is(checkPlayer1Win, 0);
});
