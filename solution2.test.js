const test = require("ava");
const solution2 = require("./solution2");

const validCaseNoRegex = /^(?:[1-9]|9)$/;
const validTestCaseRegex =
  /^([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)$/;

test("Check isValidInput for number Of test case. (valid value)", (t) => {
  const checkValidInput = solution2.checkValidInput(validCaseNoRegex, 3);
  t.is(checkValidInput, true);
});

test("Check isValidInput for number Of test case. (invalid value)", (t) => {
  const checkValidInput = solution2.checkValidInput(validCaseNoRegex, 10);
  t.is(checkValidInput, false);
});
test("Check isValidInput for valid game test case value.", (t) => {
  const checkValidInput = solution2.checkValidInput(
    validTestCaseRegex,
    "9 2 5 4"
  );
  t.is(checkValidInput, true);
});
test("Check isValidInput for invalid game test case value.", (t) => {
  const checkValidInput = solution2.checkValidInput(
    validTestCaseRegex,
    "d 2 5 4"
  );
  t.is(checkValidInput, false);
});

test("Check valid player's age.", (t) => {
  const checkValidAges = solution2.checkValidAges(3, 8);
  t.is(checkValidAges, true);
});
test("Check invalid player's age. Ages are same.", (t) => {
  const checkValidAges = solution2.checkValidAges(8, 8);
  t.is(checkValidAges, false);
});
test("Check invalid player's age. Age is 0.", (t) => {
  const checkValidAges = solution2.checkValidAges(8, 0);
  t.is(checkValidAges, false);
});

test("Check older player will win base on remaing move.", (t) => {
  const checkPlayerCanWin = solution2.checkPlayerCanWin(2, 4);
  t.is(checkPlayerCanWin, true);
});
test("Check older player will lose base on remaing move.", (t) => {
  const checkPlayerCanWin = solution2.checkPlayerCanWin(1, 5);
  t.is(checkPlayerCanWin, false);
});

test("Return optimal move 1 if only 1 move remaing.", (t) => {
  const optimalMoveNumber = solution2.optimalMoveNumber(1, true);
  t.is(optimalMoveNumber, 1);
});
test("Retrun 2 if more than 1 move is remaing and player can win.", (t) => {
  const optimalMoveNumber = solution2.optimalMoveNumber(4, true);
  t.is(optimalMoveNumber, 2);
});
test("Retrun 1 if more than 1 move is remaing and player can not win.", (t) => {
  const optimalMoveNumber = solution2.optimalMoveNumber(4, false);
  t.is(optimalMoveNumber, 1);
});

test("Older player lose when no step to begin, 0 step remaing", (t) => {
  const simulateGame = solution2.simulateGame(9, 1, 5, 2);
  t.is(simulateGame, 0);
});
test("simulateGame winnig case, 1 step remaing", (t) => {
  const simulateGame = solution2.simulateGame(9, 2, 5, 4);
  t.is(simulateGame, 1);
});
test("simulateGame wining case, 2 steps remaing", (t) => {
  const simulateGame = solution2.simulateGame(10, 1, 9, 4);
  t.is(simulateGame, 1);
});
test("simulateGame losing case, 3 steps remaing", (t) => {
  const simulateGame = solution2.simulateGame(10, 1, 9, 5);
  t.is(simulateGame, 0);
});
test("simulateGame wining case, 4 steps remaing", (t) => {
  const simulateGame = solution2.simulateGame(10, 1, 9, 6);
  t.is(simulateGame, 1);
});
