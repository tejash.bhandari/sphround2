"use strict";

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Accept number of test cases.
const numberOfTestCase = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter a number between 1 and 9: ", (answer) => {
      resolve(answer);
    });
  });
};

const validCaseNoRegex = /^(?:[1-9]|9)$/;
const validTestCaseRegex =
  /^([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)$/;

// Validate input value based in  regular expression.
const isValidInput = (validRegex, value) => {
  return validRegex.test(value);
};

// Validate age of the players
const isValidAges = (player1Age, player2Age) => {
  return player1Age > 0 && player2Age > 0 && player1Age !== player2Age;
};

// Check whether the older player wins or not.
const checkOlderPlayerWin = (player1Pos, player2Pos) => {
  const posDiffrence = Math.abs(player1Pos - player2Pos) - 1;
  const result = posDiffrence % 4;
  return result === 1 || result === 2;
};

// Check whether the player 1 wins or not.
const checkPlayer1Win = (isOlderPlayerWin, player1Age, player2Age) => {
  if (player1Age > player2Age && isOlderPlayerWin) {
    return 1;
  }
  if (player1Age < player2Age && !isOlderPlayerWin) {
    return 1;
  }
  return 0;
};

// Accept and test game test cases.
const playGame = () => {
  return new Promise((resolve, reject) => {
    rl.question("Enter game test case.: ", (gameInput) => {
      if (isValidInput(validTestCaseRegex, gameInput)) {
        const [player1Age, player1Pos, player2Age, player2Pos] = gameInput
          .split(" ")
          .map(Number);

        if (isValidAges(player1Age, player2Age)) {
          const isPlayer1Win = checkPlayer1Win(
            checkOlderPlayerWin(player1Pos, player2Pos),
            player1Age,
            player2Age
          );

          resolve(isPlayer1Win);
        }
        reject(`Invalid player age. : ${gameInput}`);
      }
      reject(`Invalid game test case. : ${gameInput}`);
    });
  });
};

// Main function
const main = async () => {
  const testCaseNo = await numberOfTestCase();
  if (isValidInput(validCaseNoRegex, testCaseNo)) {
    for (let index = 0; index < testCaseNo; index++) {
      await playGame()
        .then((result) => console.log(result))
        .catch((result) => console.log(result));
    }
  } else {
    console.log(
      `Invalid input. Please enter a number between 1 and 9 : ${testCaseNo}`
    );
  }
  rl.close();
};

// Call main function
main();

module.exports = {
  numberOfTestCase,
  validCaseNoRegex,
  validTestCaseRegex,
  isValidInput,
  isValidAges,
  checkOlderPlayerWin,
  checkPlayer1Win,
  playGame,
  main,
};
