"use strict";

const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
const validCaseNoRegex = /^(?:[1-9]|9)$/;
const validTestCaseRegex =
  /^([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)\s+([1-9][0-9]*)$/;

// Validate input value based in  regular expression.
const checkValidInput = (validRegex, value) => {
  return validRegex.test(value);
};

// Validate age of the players
const checkValidAges = (player1Age, player2Age) => {
  return player1Age > 0 && player2Age > 0 && player1Age !== player2Age;
};

// Check whether the older player wins or not.
const checkPlayerCanWin = (player1Pos, player2Pos) => {
  const posDiffrence = Math.abs(player1Pos - player2Pos) - 1;
  const result = posDiffrence % 4;
  return result === 1 || result === 2;
};

// Return optimal move based on winning chance and remaing moves
const optimalMoveNumber = (remainingSteps, canCurrentPlayerWin) => {
  if (remainingSteps === 1) {
    return 1;
  }
  if (canCurrentPlayerWin) {
    return 2;
  }
  return 1;
};

// Helper function to simulate the game
function simulateGame(player1Age, player1Pos, player2Age, player2Pos) {
  // Determine who starts first
  let currentPlayer = player1Age > player2Age ? 1 : 2;

  // Keep track of each player's remaining steps
  let remainingSteps = Math.abs(player1Pos - player2Pos) - 1;

  // Check if the current player has won
  if (remainingSteps === 0) {
    return currentPlayer === 1 ? 0 : 1;
  }

  // Play the game until one player can no longer move
  while (remainingSteps > 0) {
    const canCurrentPlayerWin = checkPlayerCanWin(player1Pos, player2Pos);

    // Determine the optimal move for the current player
    let optimalMove = optimalMoveNumber(remainingSteps, canCurrentPlayerWin);

    // Update the current player's position
    if (currentPlayer === 1) {
      player1Pos =
        player1Pos > player2Pos
          ? player1Pos - optimalMove
          : player1Pos + optimalMove;
    } else {
      player2Pos =
        player2Pos > player1Pos
          ? player2Pos - optimalMove
          : player2Pos + optimalMove;
    }

    // Recalculate remaining steps
    remainingSteps = Math.abs(player1Pos - player2Pos) - 1;

    // Check if the current player has won
    if (remainingSteps === 0) {
      return currentPlayer === 1 ? 1 : 0;
    }

    // Switch to the other player
    currentPlayer = currentPlayer === 1 ? 2 : 1;
  }

  // Alert for failure
  return -1;
}

// Accept input
const acceptInput = (message) => {
  return new Promise((resolve, reject) => {
    rl.question(message, (answer) => {
      resolve(answer);
    });
  });
};

// Main function
const main = async () => {
  // Accept number of test case
  const testCaseNo = await acceptInput("Enter a number between 1 and 9: ");
  // Validate testcase
  const isValidTestCaseNo = checkValidInput(validCaseNoRegex, testCaseNo);

  if (isValidTestCaseNo) {
    for (let index = 0; index < testCaseNo; index++) {
      // Accept game test case
      const testCaseValue = await acceptInput("Enter game test case.: ");
      // Validate game testcase
      const isValidTestCaseValue = checkValidInput(
        validTestCaseRegex,
        testCaseValue
      );

      if (isValidTestCaseValue) {
        const [player1Age, player1Pos, player2Age, player2Pos] = testCaseValue
          .split(" ")
          .map(Number);

        // Validate playes age
        const isValidAges = checkValidAges(player1Age, player2Age);
        if (isValidAges) {
          const isPlayer1Win = simulateGame(
            player1Age,
            player1Pos,
            player2Age,
            player2Pos
          );
          console.log(isPlayer1Win);
        } else {
          console.log(`Invalid player age. : ${testCaseValue}`);
        }
      } else {
        console.log(`Invalid game test case. : ${testCaseValue}`);
      }
    }
  } else {
    console.log(
      `Invalid input. Please enter a number between 1 and 9 : ${testCaseNo}`
    );
  }
  rl.close();
};

// Call main function
main();

module.exports = {
  checkValidInput,
  checkValidAges,
  checkPlayerCanWin,
  optimalMoveNumber,
  simulateGame,
  acceptInput,
  main,
};
